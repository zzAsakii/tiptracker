package org.zzasakii.tiptracker.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.NumberPicker;

import org.zzasakii.tiptracker.Activity.Dispatch;
import org.zzasakii.tiptracker.R;
import org.zzasakii.tiptracker.reference.BundleKey;


public class DispatchCount extends DialogFragment {

    private int count = 1;
    private final String TAG = "DispatchCount";
    private Bundle bundle;

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        bundle = getArguments();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_dispatch_count, null);

        NumberPicker np = (NumberPicker) view.findViewById(R.id.countPicker);
        np.setMinValue(1);
        np.setMaxValue(9);
        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                count = newVal;
            }
        });

        builder.setView(view)
                .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent intent = new Intent(getActivity(), Dispatch.class);
                        bundle.putInt(BundleKey.DISPATCH_COUNT, count);
                        intent.putExtras(bundle);
                        startActivity(intent);

                    }
                })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setTitle("Order Count");


        return builder.create();
    }

}
