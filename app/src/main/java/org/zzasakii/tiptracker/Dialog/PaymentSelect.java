package org.zzasakii.tiptracker.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.xmlpull.v1.XmlPullParser;
import org.zzasakii.tiptracker.Activity.NumberEnter;
import org.zzasakii.tiptracker.Object.OrderObject;
import org.zzasakii.tiptracker.R;
import org.zzasakii.tiptracker.reference.BundleKey;
import org.zzasakii.tiptracker.reference.DispatchStates;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by corey on 3/29/15.
 */
public class PaymentSelect extends DialogFragment {

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        final Bundle bundle = getArguments();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_payment_select, null);
        ListView paymentList = (ListView) view.findViewById(R.id.paymentList);
        final List<String> paymentArray = getPaymentTypes();
        paymentList.setAdapter(new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, paymentArray));
        paymentList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                bundle.putString(BundleKey.PAYMENT_TYPE, parent.getItemAtPosition(position).toString());
                bundle.putInt(BundleKey.REQUEST_CODE, DispatchStates.TOTAL);
                Intent intent = new Intent(getActivity(), NumberEnter.class);
                intent.putExtras(bundle);
                getActivity().startActivityForResult(intent, DispatchStates.TOTAL);
                dismiss();
            }
        });
        builder.setTitle(R.string.title_payment_select)
                .setView(view);
        return builder.create();
    }

    private List<String> getPaymentTypes() {
        List<String> payments = new ArrayList<>();
        try {
            XmlPullParser xpp = getResources().getXml(R.xml.payment_types);
            while(xpp.getEventType() != XmlPullParser.END_DOCUMENT) {
                if(xpp.getEventType() == XmlPullParser.START_TAG &&
                        xpp.getName().equals("PaymentType")) {
                    payments.add(xpp.getAttributeValue(0));
                }
                xpp.next();
            }

        } catch (Throwable t) {

        }
        return payments;
    }

}
