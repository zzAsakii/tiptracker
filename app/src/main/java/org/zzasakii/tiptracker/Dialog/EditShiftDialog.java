package org.zzasakii.tiptracker.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import org.zzasakii.tiptracker.Activity.EditShifts;
import org.zzasakii.tiptracker.Activity.MainActivity;
import org.zzasakii.tiptracker.Object.ShiftObject;
import org.zzasakii.tiptracker.R;

/**
 * Created by corey on 3/17/15.
 */
public class EditShiftDialog extends DialogFragment {

    public static final String TAG = "EditShiftDialog";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_edit_shift, null);
        final EditText name = (EditText) view.findViewById(R.id.editName);
        final EditText comp = (EditText) view.findViewById(R.id.editComp);
        final EditShifts esa = (EditShifts) getActivity();
        final ShiftObject shift = getShiftFromBundle();


        builder.setTitle("Edit Shift");
        name.setText(shift.getName());
        comp.setText(Double.toString(shift.getComp() / 100.0));


        builder.setView(view)
                .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        save(new ShiftObject(shift.getId(), name.getText().toString(),
                                ((int) (Float.parseFloat(comp.getText().toString()) * 100 + 0.5))));
                        esa.adapterUpdate();
                    }
                })
                .setNeutralButton(R.string.delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.tipDatabase.removeShift(shift.getId());
                        esa.adapterUpdate();
                    }
                });

        return builder.create();
    }

    private ShiftObject getShiftFromBundle() {
        Bundle bundle = getArguments();
        if (bundle.getParcelable("SHIFT") != null) return bundle.getParcelable("SHIFT");
        else return new ShiftObject("New", 115);
    }

    private void save(ShiftObject shift) {
        if(shift.getId() == -1) MainActivity.tipDatabase.addShift(shift);
        else MainActivity.tipDatabase.updateShift(shift.getId(), shift);
    }
}
