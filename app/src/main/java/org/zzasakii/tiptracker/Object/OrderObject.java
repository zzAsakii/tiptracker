package org.zzasakii.tiptracker.Object;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by corey on 3/11/15.
 */
public class OrderObject implements Parcelable{

    private int mdata;

    private int orderNumber;
    private long dispatchID;
    private String orderType;
    private int total;
    private int tip;
    private boolean status;

    public OrderObject() {
        this(0);
    }

    public OrderObject(int orderNumber) {
        this(orderNumber, -1, false,"Cash", 0, 0);
    }

    /**
     *
     * @param orderStatus
     * @param total
     * @param tip
     */
    public OrderObject(int orderNumber, long dispatchID, boolean orderStatus, String orderType, int total, int tip) {
        this.orderNumber = orderNumber;
        this.dispatchID = dispatchID;
        setStatus(orderStatus);
        setOrderType(orderType);
        setTotal(total);
        setTip(tip);
    }

    public String test() {
        return orderType + total + " " + tip;
    }

    public void setStatus(boolean orderStatus) {
        status = orderStatus;
    }

    public boolean getStatus() {
        return status;
    }

    public long getDispatchID() {
        return dispatchID;
    }

    public void setDispatchID(long dispatchID) {
        this.dispatchID = dispatchID;
    }

    @Override
    public String toString() {
        String statusString = (getStatus()) ? ": Complete" : "";
        return ("Order " + orderNumber + statusString);
    }

    public int getTip() {
        return tip;
    }

    public void setTip(int tip) {
        this.tip = tip;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    //For parcelable
    private OrderObject(Parcel in) {
        mdata = in.readInt();
    }

    public static final Creator<OrderObject> CREATOR = new Creator<OrderObject>() {
        @Override
        public OrderObject createFromParcel(Parcel in) {
            return new OrderObject(in);
        }

        @Override
        public OrderObject[] newArray(int size) {
            return new OrderObject[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(mdata);
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }
}
