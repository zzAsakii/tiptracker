package org.zzasakii.tiptracker.Object;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.format.Time;
import android.util.Log;

import org.zzasakii.tiptracker.ConfigHandler;
import org.zzasakii.tiptracker.reference.Config;
import org.zzasakii.tiptracker.reference.SQLTable;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by corey on 3/16/15.
 */
public class TipDatabase {

    private static final String TAG = "TipDatabase";
    private SQLiteDatabase database;
    private TipDatabaseHelper dbHelper;

    public TipDatabase(Context context, ConfigHandler configs) {
        dbHelper = new TipDatabaseHelper(context,
                configs.getConfig(Config.FieldNames.DATABASE_LOCATION) + File.separator +
                        configs.getConfig(Config.FieldNames.DATABASE_NAME));
    }

    public List<Integer> getTodaysTips() {
        Time now = new Time();
        List<Integer> tips = new ArrayList<>();

        now.setToNow();

        Cursor cursor = database.rawQuery("SELECT Tip FROM Tips JOIN Dispatch ON Tips.DispatchID = Dispatch.rowid WHERE Dispatch.DispatchDate = ? ORDER BY Tip ASC",
                new String[] {now.format(SQLTable.Dispatch.FORMAT_DATE)});

        if(cursor.moveToFirst()) {
            do {
                tips.add(cursor.getInt(0));
            } while (cursor.moveToNext());
        }

        return tips;
    }

    /**
     * Get all tips for calculating average
     * @return List of all tips
     */
    public List<Integer> getAllTips() {
        List<Integer> tips = new ArrayList<>();

        Cursor cursor = database.query(SQLTable.Order.TABLE_NAME,
                new String[] {SQLTable.Order.COLUMN_TIP}, null, null, null, null, null);

        if(cursor.moveToFirst()) {
            do {
                tips.add(cursor.getInt(0));
            } while (cursor.moveToNext());
        }

        return tips;
    }

    public long addDispatch(long shiftID, Time time){
        ContentValues values = new ContentValues();
        values.put(SQLTable.Dispatch.COLUMN_SHIFT_ID, shiftID);
        values.put(SQLTable.Dispatch.COLUMN_DATE, time.format(SQLTable.Dispatch.FORMAT_DATE));
        values.put(SQLTable.Dispatch.COLUMN_TIME, time.format(SQLTable.Dispatch.FORMAT_TIME));
        return database.insert(SQLTable.Dispatch.TABLE_NAME, null, values);
    }

    public void addOrders(List<OrderObject> orderList) {
        for (OrderObject order : orderList) {
            if (order.getStatus())
                addOrder(order);
        }
    }

    public long addOrder(OrderObject order) {
        ContentValues values = new ContentValues();
        values.put(SQLTable.Order.COLUMN_DISPATCH_ID, order.getDispatchID());
        values.put(SQLTable.Order.COLUMN_PAYMENT_TYPE, order.getOrderType());
        values.put(SQLTable.Order.COLUMN_TOTAL, order.getTotal());
        values.put(SQLTable.Order.COLUMN_TIP, order.getTip());
        return database.insert(SQLTable.Order.TABLE_NAME, null, values);
    }

    /**
     * Get shift from database by ID
     * @param id of shift
     * @return ShiftObject if exists null otherwise
     */
    public ShiftObject getShiftById(long id) {
        Cursor cursor = database.query(SQLTable.Shifts.TABLE_NAME,
                new String [] {"ROWID", SQLTable.Shifts.COLUMN_NAME, SQLTable.Shifts.COLUMN_COMP},
                "ROWID = ?", new String[] {Long.toString(id)}, null, null, null);
        if (cursor.moveToFirst()) {
            return new ShiftObject(cursor.getLong(0),cursor.getString(1), cursor.getInt(2));
        } else {
            return null;
        }
    }

    /**
     * Update an existing shift
     * @param id of shift to update, if -1 redirects to add shift
     * @param shift New shift information
     */
    public void updateShift(Long id, ShiftObject shift) {
        String sql = "UPDATE " + SQLTable.Shifts.TABLE_NAME +
                " SET " + SQLTable.Shifts.COLUMN_NAME + "=" + "'" + shift.getName() + "'" + ", " +
                SQLTable.Shifts.COLUMN_COMP + "=" + shift.getComp() +
                " WHERE ROWID=" + id + ";";
        database.execSQL(sql);
    }

    /**
     * Add a new shift
     * @param shift shift to add
     * @return ID of new shift
     */
    public long addShift(ShiftObject shift) {
        ContentValues values = new ContentValues(2);
        values.put(SQLTable.Shifts.COLUMN_NAME, shift.getName());
        values.put(SQLTable.Shifts.COLUMN_COMP, shift.getComp());
        return database.insert(SQLTable.Shifts.TABLE_NAME, null, values);
    }

    public List<ShiftObject> getAllShifts() {
        List<ShiftObject> shifts = new ArrayList<ShiftObject>();
        Cursor cursor = database.query(SQLTable.Shifts.TABLE_NAME,
                new String[] {"ROWID", SQLTable.Shifts.COLUMN_NAME, SQLTable.Shifts.COLUMN_COMP},
                null, null, null, null, null);
        Log.d(TAG, "After query");
        if (cursor.moveToFirst()) {
            do {
                Log.d(TAG, "iterate");
                shifts.add(new ShiftObject(cursor.getLong(0),
                        cursor.getString(1), cursor.getInt(2)));
            } while (cursor.moveToNext());
        }

        return shifts;
    }

    //TODO add field to hide shifts, removing can ruin the database
    public int removeShift(long id) {
        return -1;
    }

    public void open() {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        database.close();
    }

    //Increment on database scheme change
    public static final int DATABASE_VERSION = 1;

    public class TipDatabaseHelper extends SQLiteOpenHelper {

        TipDatabaseHelper(Context context, String dbName) {
            super(context, dbName, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQLTable.Shifts.CREATE_TABLE);
            db.execSQL(SQLTable.Dispatch.CREATE_TABLE);
            db.execSQL(SQLTable.Order.CREATE_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }

    }
}
