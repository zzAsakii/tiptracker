package org.zzasakii.tiptracker.Object;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by corey on 3/20/15.
 */
public class ShiftObject implements Parcelable {

    private int mdata;

    private String name;
    private int comp;
    private long id;

    public ShiftObject(String name, int comp) {
        this(-1, name, comp);
    }

    public ShiftObject(long id, String name, int comp) {
        setId(id);
        setName(name);
        setComp(comp);
    }

    /**
     * get id if set
     * @return ID -1 if not set
     */
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getComp() {
        return comp;
    }

    public void setComp(int comp) {
        this.comp = comp;
    }

    @Override
    public String toString() {
        return getName();
    }



    //For parcelable
    private ShiftObject(Parcel in) {
        mdata = in.readInt();
    }

    public static final Creator<ShiftObject> CREATOR = new Creator<ShiftObject>() {
        @Override
        public ShiftObject createFromParcel(Parcel in) {
            return new ShiftObject(in);
        }

        @Override
        public ShiftObject[] newArray(int size) {
            return new ShiftObject[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(mdata);
    }
}
