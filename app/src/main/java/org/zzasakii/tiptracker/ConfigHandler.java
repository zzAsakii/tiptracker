package org.zzasakii.tiptracker;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import org.zzasakii.tiptracker.reference.Config;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by corey on 3/14/15.
 */
public class ConfigHandler {

    private static final String TAG = "ConfigHandler";
    private File configFile;
    private HashMap<String, String> configMap;

    public ConfigHandler(Context context) {
        configFile = new File(context.getFilesDir(), Config.CONFIG_FILE_NAME);
        configMap = new HashMap<String,String>();
        if(!configFile.exists()) {
            setDefaultValues();
            writeConfig();
        } else {
            updateConfigMap();
        }

    }

    private void setDefaultValues() {
        configMap.put(Config.FieldNames.DATABASE_LOCATION,
                Environment.getExternalStorageDirectory().toString() + File.separator + "TipTracker");
        configMap.put(Config.FieldNames.DATABASE_NAME, Config.DefaultValues.DATABASE_NAME);
    }

    /**
     * Updates configMap from file
     * @return true on success false on failure
     */
    public boolean updateConfigMap() {
        CSVReader reader = null;
        List<String[]> readList = null;
        try{
            reader = new CSVReader(new FileReader(configFile));
            readList = reader.readAll();
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(readList != null) {
            configMap.clear();
            Iterator<String[]> iterator = readList.iterator();
            String[] tmp;
            while(iterator.hasNext()) {
                tmp = iterator.next();
                configMap.put(tmp[0], tmp[1]);
            }

            return true;
        } else {
            Log.e(TAG, "Failed to read list");
            return false;
        }

    }

    /**
     * Get config value from configMap
     * @param configField Field of value to be returned
     * @return Value of field
     */
    public String getConfig(String configField) {
        return configMap.get(configField);
    }

    /**
     * Sets a config, and write to disk
     * @param configField Field to set
     * @param newValue New value of field
     */
    public void setConfig(String configField, String newValue) {
        configMap.put(configField, newValue);
        writeConfig();
    }

    private void writeConfig() {
        CSVWriter writer = null;
        List<String[]> writeList = new ArrayList<String[]>();

        writeList.add(new String[]{Config.FieldNames.DATABASE_LOCATION,
                configMap.get(Config.FieldNames.DATABASE_LOCATION)});

        writeList.add(new String[]{Config.FieldNames.DATABASE_NAME,
                configMap.get(Config.FieldNames.DATABASE_NAME)});

        try{
            writer = new CSVWriter(new FileWriter(configFile, false));
            writer.writeAll(writeList);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
