package org.zzasakii.tiptracker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import org.zzasakii.tiptracker.Activity.EditShifts;
import org.zzasakii.tiptracker.Activity.Settings;
import org.zzasakii.tiptracker.Dialog.EditShiftDialog;

import static android.support.v4.app.ActivityCompat.startActivity;

/**
 * Created by corey on 4/13/15.
 */
public class MenuHandler {

    public static boolean handleMenuID(Activity activity, int id, String TAG) {
        boolean success = false;

        switch(id) {
            case R.id.action_settings:
                activity.startActivity(new Intent(activity, Settings.class));
                success = true;
                break;
            case R.id.action_edit_shift:
                activity.startActivity(new Intent(activity, EditShifts.class));
                success = true;
                break;
            case R.id.addShift:
                EditShiftDialog dialog = new EditShiftDialog();
                dialog.setArguments(new Bundle());
                dialog.show(activity.getFragmentManager(), TAG);
        }

        return success;
    }

}
