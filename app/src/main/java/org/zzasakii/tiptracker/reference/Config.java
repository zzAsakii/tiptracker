package org.zzasakii.tiptracker.reference;

/**
 * Created by corey on 3/13/15.
 */
public class Config {


    public static final String CONFIG_FILE_NAME = "config.csv";

    public class DefaultValues {
        public static final String DATABASE_NAME = "database.db";
    }

    public class FieldNames {
        public static final String DATABASE_LOCATION = "databaseLocation";
        public static final String DATABASE_NAME = "databaseName";
    }

}
