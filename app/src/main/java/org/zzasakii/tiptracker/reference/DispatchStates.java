package org.zzasakii.tiptracker.reference;

/**
 * Created by corey on 3/9/15.
 */
public class DispatchStates {
    public static final int TOTAL = 1;
    public static final int RECEIVED = 2;
    public static final int TIP = 3;
    public static final int CHANGE = 4;
}
