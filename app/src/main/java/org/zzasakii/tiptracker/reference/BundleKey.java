package org.zzasakii.tiptracker.reference;

/**
 * Created by corey on 3/29/15.
 */
public class BundleKey {
    //TODO make subclass
    public static final String DISPATCH_COUNT = "DISPATCH_COUNT";
    public static final String PAYMENT_TYPE = "PAYMENT_TYPE";
    public static final String SHIFTID = "SHIFTID";
    public static final String WORKING_ORDER_ID = "WORKING_ORDER_ID";
    public static final String REQUEST_CODE = "REQUEST_CODE";
    public static final String TOTAL = "TOTAL";
    public static final String TIP = "TIP";
    public static final String CHANGE = "CHANGE";
}
