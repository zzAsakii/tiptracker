package org.zzasakii.tiptracker.reference;

/**
 * Created by corey on 3/17/15.
 */
public class SQLTable {

    public class Shifts {
        public static final String TABLE_NAME = "Shifts";

        public static final String COLUMN_ID = "ShiftID";
        public static final String COLUMN_NAME = "ShiftName";
        public static final String COLUMN_COMP = "DriverComp";

        public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" +
                COLUMN_ID + " INT PRIMARY_KEY, " +
                COLUMN_NAME + " TEXT, " +
                COLUMN_COMP + " INT);";
    }

    public class Dispatch {
        public static final String TABLE_NAME = "Dispatch";

        public static final String FORMAT_DATE = "%F-%w";
        public static final String FORMAT_TIME = "%H:%M";

        public static final String COLUMN_ID = "DispatchID";
        public static final String COLUMN_SHIFT_ID = Shifts.COLUMN_ID;
        public static final String COLUMN_DATE = "DispatchDate";
        public static final String COLUMN_TIME = "DispatchTime";

        public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" +
                COLUMN_ID + " INT PRIMARY_KEY, " +
                COLUMN_SHIFT_ID + " INT, " +
                COLUMN_DATE + " TEXT, " +
                COLUMN_TIME + " TEXT);";
    }

    public class Order {
        public static final String TABLE_NAME = "Tips";

        public static final String COLUMN_ID = "OrderID";
        public static final String COLUMN_DISPATCH_ID = Dispatch.COLUMN_ID;
        public static final String COLUMN_PAYMENT_TYPE = "PaymentType";
        public static final String COLUMN_TOTAL = "Total";
        public static final String COLUMN_TIP = "Tip";

        public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" +
                COLUMN_ID + " INT PRIMARY_KEY, " +
                COLUMN_DISPATCH_ID + " INT, " +
                COLUMN_PAYMENT_TYPE + " TEXT, " +
                COLUMN_TOTAL + " INT, " +
                COLUMN_TIP + " INT);";
    }

}
