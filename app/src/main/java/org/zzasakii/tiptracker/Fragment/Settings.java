package org.zzasakii.tiptracker.Fragment;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import org.zzasakii.tiptracker.R;

public class Settings extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);
    }

}
