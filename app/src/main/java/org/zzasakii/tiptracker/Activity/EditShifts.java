package org.zzasakii.tiptracker.Activity;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.zzasakii.tiptracker.Dialog.EditShiftDialog;
import org.zzasakii.tiptracker.MenuHandler;
import org.zzasakii.tiptracker.R;
import org.zzasakii.tiptracker.Object.ShiftObject;

import java.util.List;


public class EditShifts extends ActionBarActivity {

    public static final String TAG = "EditShifts";
    private ListView listView;
    private List<ShiftObject> shifts;
    private ArrayAdapter<ShiftObject> shiftAdapter;
    private boolean isAdapterSet = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_shifts);
        listView = (ListView) findViewById(R.id.ShiftsList);
        shifts = MainActivity.tipDatabase.getAllShifts();
        shiftAdapter = new ArrayAdapter<ShiftObject>(this,
                android.R.layout.simple_list_item_1, shifts);
        listView.setEmptyView(findViewById(R.id.emptyList));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                EditShiftDialog dialog = new EditShiftDialog();
                Bundle bundle = new Bundle();
                bundle.putParcelable("SHIFT", shifts.get(position));
                dialog.setArguments(bundle);
                dialog.show(getFragmentManager(), TAG);
            }
        });
        listView.setAdapter(shiftAdapter);
    }

    public void adapterUpdate() {
        shifts.clear();
        shifts.addAll(MainActivity.tipDatabase.getAllShifts());
        shiftAdapter.notifyDataSetChanged();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_shifts, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
            return MenuHandler.handleMenuID(this, id, TAG);
        }
    }
