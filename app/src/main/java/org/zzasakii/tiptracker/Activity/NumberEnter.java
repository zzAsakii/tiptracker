package org.zzasakii.tiptracker.Activity;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import org.zzasakii.tiptracker.R;
import org.zzasakii.tiptracker.reference.BundleKey;
import org.zzasakii.tiptracker.reference.DispatchStates;


public class NumberEnter extends ActionBarActivity {

    private String enteredNumberString;
    private TextView enternum;
    private int requestCode;
    private String paymentType;
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_number_enter);
        enteredNumberString = "";
        bundle = getIntent().getExtras();
        requestCode = bundle.getInt(BundleKey.REQUEST_CODE);
        paymentType = bundle.getString(BundleKey.PAYMENT_TYPE);
        titleHandler();
        enternum = (TextView) findViewById(R.id.enternum);
        ImageButton backspace = (ImageButton) findViewById(R.id.numpadBack);
        backspace.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                wipeNum();
                return false;
            }
        });
    }

    private void titleHandler() {
        switch (requestCode) {
            case DispatchStates.TOTAL:
                setTitle("Total");
                break;
            case DispatchStates.TIP:
                setTitle("Tip");
                break;
            case DispatchStates.RECEIVED:
                setTitle("Received");
                break;
            case DispatchStates.CHANGE:
                setTitle("Change");
                break;
            default:
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void next(View view) {
        if (paymentType.equals("Credit")) {
            switch (requestCode) {
                case DispatchStates.TOTAL:
                    startNext(DispatchStates.TIP);
                    break;
                case DispatchStates.TIP:
                    collapseStack(new Intent());
                    break;
            }
        } else if(paymentType.equals("Cash")) {
            switch (requestCode) {
                case DispatchStates.TOTAL:
                    startNext(DispatchStates.RECEIVED);
                    break;
                case DispatchStates.RECEIVED:
                    startNext(DispatchStates.CHANGE);
                    break;
                case DispatchStates.CHANGE:
                    collapseStack(new Intent());
                    break;
            }
        }
    }

    private void collapseStack(Intent data) {
        switch (requestCode) {
            case DispatchStates.TIP:
                data.putExtra(BundleKey.TIP, enteredToInt());
                break;
            case DispatchStates.CHANGE:
                data.putExtra(BundleKey.CHANGE, enteredToInt());
                break;
            case DispatchStates.RECEIVED:
                data.putExtra(BundleKey.CHANGE, (enteredToInt() - data.getIntExtra(BundleKey.CHANGE, 0)));
            //total will always be last so all items that must be added will be added here to avoid redundancy
            case DispatchStates.TOTAL:
                data.putExtra(BundleKey.TOTAL, enteredToInt());
                if(paymentType.equals("Cash")) data.putExtra(BundleKey.TIP,
                        (data.getIntExtra(BundleKey.CHANGE, 0) - data.getIntExtra(BundleKey.TOTAL, 0)));
                data.putExtra(BundleKey.WORKING_ORDER_ID, bundle.getInt(BundleKey.WORKING_ORDER_ID));
                data.putExtra(BundleKey.PAYMENT_TYPE, bundle.getString(BundleKey.PAYMENT_TYPE));
                break;
        }
        if(getParent() == null) {
            setResult(RESULT_OK, data);
        } else {
            getParent().setResult(RESULT_OK, data);
        }
        finish();
    }

    public void startNext(int nextCode) {
        Intent intent = new Intent(this, NumberEnter.class);
        bundle.putInt(BundleKey.REQUEST_CODE, nextCode);
        intent.putExtras(bundle);
        startActivityForResult(intent, nextCode);
    }

    public void numpadHandler(View view) {
        switch(view.getId()) {

            case R.id.numpad0:
                appendNum("0");
                break;

            case R.id.numpad00:
                appendNum("00");
                break;

            case R.id.numpad1:
                appendNum("1");
                break;

            case R.id.numpad2:
                appendNum("2");
                break;

            case R.id.numpad3:
                appendNum("3");
                break;

            case R.id.numpad4:
                appendNum("4");
                break;

            case R.id.numpad5:
                appendNum("5");
                break;

            case R.id.numpad6:
                appendNum("6");
                break;

            case R.id.numpad7:
                appendNum("7");
                break;

            case R.id.numpad8:
                appendNum("8");
                break;

            case R.id.numpad9:
                appendNum("9");
                break;

            case R.id.numpadBack:
                trimNum();
                break;

            default:
        }
    }

    private void renderNum() {
        String prepared = "$";
        switch (enteredNumberString.length()) {
            case 0:
                prepared += "0.00";
                break;
            case 1:
                prepared += "0.0" + enteredNumberString;
                break;
            case 2:
                prepared += "0." + enteredNumberString;
                break;
            default:
                prepared += enteredNumberString.substring(0,enteredNumberString.length()-2)
                        + "." + enteredNumberString.substring(enteredNumberString.length()-2);
        }
        enternum.setText(prepared);
    }

    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        if (resultCode == RESULT_OK) collapseStack(data);
    }

    private boolean startsWithSane() {
        if (enteredNumberString.startsWith("0"))
            return false;
        return true;
    }

    private void appendNum(String ap) {
        enteredNumberString += ap;
        while (!startsWithSane()) {
            enteredNumberString = enteredNumberString.substring(1);
        }
        renderNum();
    }

    private void trimNum() {
        if(enteredNumberString.length() > 0)
            enteredNumberString = enteredNumberString.substring(0,enteredNumberString.length() - 1);
        renderNum();
    }

    private void wipeNum() {
        enteredNumberString = "";
        renderNum();
    }

    public int enteredToInt() {
        if (enteredNumberString.length() > 0) {
            return (Integer.parseInt(enteredNumberString));
        } else {
            return 0;
        }
    }
}
