package org.zzasakii.tiptracker.Activity;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.zzasakii.tiptracker.R;

import java.util.List;

public class ViewData extends ActionBarActivity {

    public static final String TAG = "ViewData";

    private int globalAverage;
    private int dayavg;
    private int dayBest;
    private int todayTotal;
    private int todaysMedian;
    private int todaysZeros;
    private TextView dayAverage;
    private TextView plusMinusGlobal;
    private TextView dayTotal;
    private TextView todaysBest;
    private TextView todaysMedianView;
    private TextView zeros;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_data);
        dayAverage = (TextView) findViewById(R.id.dayAverage);
        plusMinusGlobal = (TextView) findViewById(R.id.plusMinusGlobal);
        dayTotal = (TextView) findViewById(R.id.totalForDay);
        todaysBest = (TextView) findViewById(R.id.todaysBest);
        todaysMedianView = (TextView) findViewById(R.id.dayMedian);
        zeros = (TextView) findViewById(R.id.Zeros);
    }

    @Override
    protected void onResume() {
        super.onResume();
        List<Integer> tdtips = MainActivity.tipDatabase.getTodaysTips();
        dayBest = max(tdtips);
        dayavg = avgTip(tdtips);
        todayTotal = total(tdtips);
        globalAverage = avgTip(MainActivity.tipDatabase.getAllTips());
        todaysMedian = median(tdtips);
        todaysZeros = getZeros(tdtips);

        zeros.setText("" + todaysZeros);
        todaysMedianView.setText("" + (double)todaysMedian / 100);
        todaysBest.setText("" + (double)dayBest / 100);
        dayAverage.setText("" + (double)dayavg / 100);
        plusMinusGlobal.setText("" + ((double)(dayavg - globalAverage) / 100));
        dayTotal.setText("" + (double)todayTotal / 100);
    }

    private int total(List<Integer> tips) {
        int total = 0;

        for(int currentTip: tips) {
            total += currentTip;
        }

        return total;
    }

    /**
     * get number of zeros and unintentional tips
     * @param tips
     * @return Number of tips less than $1
     */
    private int getZeros(List<Integer> tips) {
        int totalZeros = 0;

        for(int tip: tips) {
            if(tip < 100) {
                totalZeros++;
            }
        }

        return totalZeros;
    }

    private int avgTip(List<Integer> tips) {
        int average = 0;

        double tipLength = tips.size();
        double currentavg = 0;

        for(int tip: tips) {
            currentavg += ((double)tip / tipLength);
        }

        average = (int) currentavg;

        return average;
    }

    private int median(List<Integer> tips) {
        if(tips.size() % 2 == 0) {
            int average = tips.get(tips.size() / 2);
            average += tips.get((tips.size() - 2) / 2);
            return average/2;
        } else {
            return tips.get((tips.size() - 1) / 2);
        }
    }

    private int max(List<Integer> tips) {
        int bestTip = 0;

        if(tips.get(0) != null)
            bestTip = tips.get(tips.size()-1);

        return bestTip;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_data, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.edit_orders) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
