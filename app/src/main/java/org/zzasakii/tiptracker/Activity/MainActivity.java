package org.zzasakii.tiptracker.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import org.zzasakii.tiptracker.ConfigHandler;
import org.zzasakii.tiptracker.Dialog.DispatchCount;
import org.zzasakii.tiptracker.R;
import org.zzasakii.tiptracker.Object.ShiftObject;
import org.zzasakii.tiptracker.Object.TipDatabase;
import org.zzasakii.tiptracker.reference.BundleKey;
import org.zzasakii.tiptracker.reference.RequestCodes;

import org.zzasakii.tiptracker.MenuHandler;


public class MainActivity extends ActionBarActivity {

    public static final String TAG = "MainActivity";

    public static Context context;
    private ShiftObject shift;
    private ConfigHandler configs;
    public static volatile TipDatabase tipDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = getApplicationContext();
        configs = new ConfigHandler(context);
        tipDatabase = new TipDatabase(context, configs);
        tipDatabase.open();
        shift = null;
    }

    public void data(View view) {
        startActivity(new Intent(this, ViewData.class));
    }

    @Override
    protected void onDestroy () {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return MenuHandler.handleMenuID(this, id, TAG);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RequestCodes.SELECT_SHIFT) {
            if (resultCode == RESULT_OK) {
                shift = tipDatabase.getShiftById(data.getLongExtra("ID", -1));
                Button button = (Button) findViewById(R.id.shiftButton);
                button.setText(context.getString(R.string.end_shift) + shift.getName());
            }
        }
    }

    public void startShift(View view) {
        if(shift == null) {
            Intent intent = new Intent(this, SelectShift.class);
            startActivityForResult(intent, RequestCodes.SELECT_SHIFT);
        } else {
            shift = null;
            Button button = (Button) findViewById(R.id.shiftButton);
            button.setText(R.string.start_shift);
        }
    }

    public void dispatch(View view) {
        if(shift == null) {
            //TODO add no shift selected dialog
        } else {
            DispatchCount dialog = new DispatchCount();
            Bundle bundle = new Bundle();
            Log.d(TAG, "" + shift.getId());
            bundle.putLong(BundleKey.SHIFTID, shift.getId());
            dialog.setArguments(bundle);
            dialog.show(getFragmentManager(), TAG);
        }
    }
}
