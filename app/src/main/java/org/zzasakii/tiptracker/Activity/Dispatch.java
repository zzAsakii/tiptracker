package org.zzasakii.tiptracker.Activity;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import org.zzasakii.tiptracker.Dialog.PaymentSelect;
import org.zzasakii.tiptracker.Object.OrderObject;
import org.zzasakii.tiptracker.R;
import org.zzasakii.tiptracker.reference.BundleKey;
import org.zzasakii.tiptracker.reference.SQLTable;

import java.util.ArrayList;
import java.util.List;


public class Dispatch extends ActionBarActivity implements AdapterView.OnItemSelectedListener {

    private static final String TAG = "DispatchActivity";
    List<OrderObject> orderList;
    private Bundle bundle;
    private Time now;
    private ArrayAdapter<OrderObject> orderAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dispatch);
        bundle = getIntent().getExtras();
        int orders = bundle.getInt(BundleKey.DISPATCH_COUNT, 1);
        ListView dispatchList = (ListView) findViewById(R.id.dispatchList);
        orderList = new ArrayList<OrderObject>();
        now = new Time();
        now.setToNow();

        Log.d(TAG, "Variables declared");

        for (int i = 1; i <= orders; i++) {
            orderList.add(new OrderObject(i));
        }

        orderAdapter = new ArrayAdapter<OrderObject>(this,
                android.R.layout.simple_list_item_1, orderList);

        dispatchList.setAdapter(orderAdapter);
        dispatchList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    bundle.putInt(BundleKey.WORKING_ORDER_ID, position);
                    PaymentSelect dialog = new PaymentSelect();
                    dialog.setArguments(bundle);
                    dialog.show(getFragmentManager(), TAG);
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void endDispatch(View view) {
        long dID = MainActivity.tipDatabase.addDispatch(bundle.getLong(BundleKey.SHIFTID), now);
        for (OrderObject order : orderList) {
            order.setDispatchID(dID);
        }
        MainActivity.tipDatabase.addOrders(orderList);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            orderList.set(data.getIntExtra(BundleKey.WORKING_ORDER_ID, 0),
                    new OrderObject(data.getIntExtra(BundleKey.WORKING_ORDER_ID, 0) + 1, -1, true, data.getStringExtra(BundleKey.PAYMENT_TYPE), data.getIntExtra(BundleKey.TOTAL, 0), data.getIntExtra(BundleKey.TIP, 0)));
            orderAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
