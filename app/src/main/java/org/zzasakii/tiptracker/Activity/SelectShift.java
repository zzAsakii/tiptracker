package org.zzasakii.tiptracker.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.zzasakii.tiptracker.MenuHandler;
import org.zzasakii.tiptracker.R;
import org.zzasakii.tiptracker.Object.ShiftObject;

import java.util.List;

/**
 * Created by corey on 3/24/15.
 */
public class SelectShift extends ActionBarActivity {

    public static final String TAG = "SelectShift";

    private List<ShiftObject> shifts;
    private ArrayAdapter<ShiftObject> shiftAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_shift);
        shifts = MainActivity.tipDatabase.getAllShifts();
        shiftAdapter = new ArrayAdapter<ShiftObject>(this,
                android.R.layout.simple_list_item_1, shifts);
        ListView listView = (ListView) findViewById(R.id.select_shift);
        listView.setEmptyView(findViewById(R.id.select_shift_empty));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent data = new Intent();
                data.putExtra("ID", shifts.get(position).getId());
                if(getParent() == null) {
                    setResult(RESULT_OK, data);
                } else {
                    getParent().setResult(RESULT_OK, data);
                }
                finish();
            }
        });
        listView.setAdapter(shiftAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return MenuHandler.handleMenuID(this, id, TAG);
    }

}
